package ru.spring.models;

import javax.validation.constraints.*;

public class Person {
    private int id;

    @NotEmpty(message = "Name not is empty")
    @Size(min = 2, max = 30, message = "Name 2 - 30 char")
    private String name;

    @Min(value = 0, message = "age > 0")
    private int age;

    @NotEmpty(message = "Email not is empty")
    @Email(message = "Email is not valid")
    private String email;

    @Pattern(regexp = "[A-Z]\\w+, [A-Z]\\w+, \\d{6}", message = "Address has been pattern: Country, City, index(6 num)")
    private String address;

    public Person(int id, String name, int age, String email, String address) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.email = email;
        this.address = address;
    }

    public Person() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
